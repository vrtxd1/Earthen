# Earthen: Dirt Simulator

**Earthen: Dirt Simulator** is a free/gratis/libre open-source digital game about solarpunk gardening and soil life. 🌱

Earthen is built on Godot 4, the free/gratis/libre open-source game engine. https://godotengine.org/

**More info & concept art:** https://vrtxd.wordpress.com/earthen

**Design:** vrtxd (Juho Hartikainen) 2024- https://linktr.ee/vrtxd

**License:** MIT https://opensource.org/license/MIT