## This file is a part of the Godot 4 source code of
## Earthen: Dirt Simulator https://vrtxd.wordpress.com/earthen
## Design: vrtxd (Juho Hartikainen) 2024-
## License: MIT https://opensource.org/license/MIT

class_name Atom
extends Node


var chem_element : ChemElement.Symbol # atomic number: protons
var neutrons : int # mass number - protons
var quantity : int # number of atoms, right subscript
var charge : int # electric charge: protons - electrons
var sux_chrs := [ # superscript & subscript lookup table
	"0123456789+-=()",
	"⁰¹²³⁴⁵⁶⁷⁸⁹⁺⁻⁼⁽⁾",
	"₀₁₂₃₄₅₆₇₈₉₊₋₌₍₎",
]


enum Component {
	PROTONS, # proton number: element number (left subscript, redundant)
	NUCLEONS, # mass number: number of nucleons (left superscript)
	ELEMENT, # chemical element symbol (middle characters)
	QUANTITY, # quantity: number of atoms (right subscript)
	CHARGE, # electric charge: proton-electron balance (right superscript)
}


## Create a new atom based on a chemical formula.
func read_formula(formula : String):
	#print(str("formula: ", formula))
	var components := ["","","","",""]
	var phase := Component.NUCLEONS
	for chr in formula:
		if chr in sux_chrs[1]:
			if phase == Component.NUCLEONS: pass
			else: phase = Component.CHARGE
			components[phase] += read_suxscript(chr)
		elif chr in "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz":
			phase = Component.ELEMENT
			components[phase] += chr
		elif chr in sux_chrs[2]:
			phase = Component.QUANTITY
			components[phase] += read_suxscript(chr)
	if components[Component.QUANTITY].length() == 0:
		components[Component.QUANTITY] += "1"
	if components[Component.CHARGE].length() == 1:
		components[Component.CHARGE] += "1"

	#print(components)

	chem_element = ChemElement.Symbol.get(components[Component.ELEMENT])
	if components[Component.NUCLEONS].length() != 0:
		neutrons = components[Component.NUCLEONS] as int - chem_element
	else:
		neutrons = -1
	quantity = components[Component.QUANTITY] as int
	charge = components[Component.CHARGE] as int

	#print(str("element/protons: ", chem_element))
	#if components[Component.NUCLEONS].length() != 0:
		#print(str("neutrons: ", neutrons))
	#else:
		#print(str("neutrons: (unknown)"))
	#print(str(
		#"nucleons: ", get_nucleons(),
		#"\nquantity: ", quantity,
		#"\ncharge: ", charge,
		#"\nelectrons: ", get_electrons(),
	#))


func read_suxscript(string : String) -> String:
	var output := ""
	var index := 0
	for chr in string:
		if chr in sux_chrs[1]: index = 1 # superscript
		if chr in sux_chrs[2]: index = 2 # subscript
		output += sux_chrs[0][sux_chrs[index].find(chr)]
	if output[-1] in "+-": # electric charge
		output = (output[-1] + output).left(-1)
	return output


func get_nucleons() -> int:
	return chem_element + neutrons


func get_electrons() -> int:
	return chem_element - charge
