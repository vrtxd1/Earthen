## This file is a part of the Godot 4 source code of
## Earthen: Dirt Simulator https://vrtxd.wordpress.com/earthen
## Design: vrtxd (Juho Hartikainen) 2024-
## License: MIT https://opensource.org/license/MIT

extends Node


@export var main_menu: PackedScene

@export_group("GUI Themes")
@export var gui_theme_dark: Theme
@export var gui_theme_light: Theme

var current_scene: Node = null


func _ready():
	# Could not get ProjectSettings.set_setting to change the active theme
	# at runtime, so the active theme is stored in the root window instead.
	var current_gui_theme = load(ProjectSettings.get_setting("gui/theme/custom"))
	get_window().theme = current_gui_theme
	var root = get_tree().root
	current_scene = root.get_child(root.get_child_count() - 1)


func change_scene_to_file(path: String):
	call_deferred("_deferred_change_scene_to_file", path)


func change_scene_to_packed(scene: PackedScene):
	call_deferred("_deferred_change_scene_to_packed", scene)
	pass


func toggle_gui_theme():
	if get_window().theme == gui_theme_dark:
		get_window().theme = gui_theme_light
		RenderingServer.set_default_clear_color(Color(1.0, 0.973, 0.941))
	else:
		get_window().theme = gui_theme_dark
		RenderingServer.set_default_clear_color(Color(0.06, 0.06, 0.06))


func _deferred_change_scene_to_file(path: String):
	current_scene.free()
	var new_scene = ResourceLoader.load(path)
	current_scene = new_scene.instantiate()
	get_tree().root.add_child(current_scene)
	get_tree().current_scene = current_scene # SceneTree compatibility


func _deferred_change_scene_to_packed(scene: PackedScene):
	current_scene.free()
	current_scene = scene.instantiate()
	get_tree().root.add_child(current_scene)
	get_tree().current_scene = current_scene # SceneTree compatibility
