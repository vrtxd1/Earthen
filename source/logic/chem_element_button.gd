## This file is a part of the Godot 4 source code of
## Earthen: Dirt Simulator https://vrtxd.wordpress.com/earthen
## Design: vrtxd (Juho Hartikainen) 2024-
## License: MIT https://opensource.org/license/MIT

class_name ChemElementButton
extends Button


signal chem_element_button_pressed(chem_symbol : int)

@export var chem_element := ChemElement.Symbol.Mg


func _ready():
	pressed.connect(_on_chem_element_button_pressed.bind(chem_element))


func _on_chem_element_button_pressed(chem_symbol):
	chem_element_button_pressed.emit(chem_symbol)
