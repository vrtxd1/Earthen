## This file is a part of the Godot 4 source code of
## Earthen: Dirt Simulator https://vrtxd.wordpress.com/earthen
## Design: vrtxd (Juho Hartikainen) 2024-
## License: MIT https://opensource.org/license/MIT

class_name PeriodicTable
extends Control
## Periodic table of chemical elements.


@export_group("GUI Nodes")

@export var button_parent : GridContainer # Parent node for element buttons
@export var name_label : Label # Element name
@export var std_atom_wght_label : Label
@export var debug_label : Label

@export_group("Colors")

@export_subgroup("s-block")
@export var block0_dark : Color
@export var block0_text_dark : Color
@export var block0_light : Color
@export var block0_text_light : Color
@export_subgroup("p-block")
@export var block1_dark : Color
@export var block1_text_dark : Color
@export var block1_light : Color
@export var block1_text_light : Color
@export_subgroup("d-block")
@export var block2_dark : Color
@export var block2_text_dark : Color
@export var block2_light : Color
@export var block2_text_light : Color
@export_subgroup("f-block")
@export var block3_dark : Color
@export var block3_text_dark : Color
@export var block3_light : Color
@export var block3_text_light : Color
@export_subgroup("g-block")
@export var block4_dark : Color
@export var block4_text_dark : Color
@export var block4_light : Color
@export var block4_text_light : Color

@export_category("Instancing Scenes")
@export var chem_element_button : PackedScene
@export var button_gap : PackedScene

var chem_elements_data : Array
var symbol_id : int
var std_atom_wght_id : int
var name_en_us_id : int
var name_fi_fi_id : int
var current_atom_no : int


func _ready():
	chem_elements_data = DataFileAccess.new().read_csv_data(
			"res://data/chem_elements.csv")
	chem_elements_data = unpack_data_array(chem_elements_data)
	assign_data_ids(chem_elements_data)
	create_periodic_table() # Instantiate and add element buttons to the scene


func unpack_data_array (data : Array):
	var unpacked_data : Array
	for row in data:
		#print(row)
		var unpacked_row : Array
		for cell in row:
			var unpacked_cell : String
			for string in cell:
				unpacked_cell = string
			unpacked_row.push_back(unpacked_cell)
		unpacked_data.push_back(unpacked_row)
		#print(str("→ ", unpacked_row))
	return unpacked_data


func assign_data_ids (data : Array):
	var front = data.front()
	symbol_id = front.find("symbol")
	std_atom_wght_id = front.find("stdAtomWght")
	name_en_us_id = front.find("name_enUS")
	name_fi_fi_id = front.find("name_fiFI")


# Instantiates a chemical element button 
func create_chem_element_button(atom_no : int):
	var button : ChemElementButton = chem_element_button.instantiate()
	var chem_element_data = chem_elements_data[atom_no]
	button.chem_element = atom_no as ChemElement.Symbol
	button.name = str("ChemElementButton ", chem_element_data[symbol_id])
	button.text = chem_element_data[symbol_id]

	#var res_path : String = str(
			#"res://visuals/patches/patch-chem_element_button-normal-dark.tres")
	#var texture = load(res_path)
	#button.set("theme_override_styles/normal", texture)
	
	var color : Color
	match get_block(atom_no):
		0: color = block0_dark
		1: color = block1_dark
		2: color = block2_dark
		3: color = block3_dark
	#texture.set_modulate(color)
	match get_block(atom_no):
		0: color = block0_text_dark
		1: color = block1_text_dark
		2: color = block2_text_dark
		3: color = block3_text_dark
	button.set("theme_override_colors/font_color", color)

	connect_button_signal(button)
	button_parent.add_child(button)


func create_button_gap(gaps : int):
	while gaps > 0:
		var new_button_gap = button_gap.instantiate()
		button_parent.add_child(new_button_gap)
		gaps -= 1


# Instantiates and layouts all the chemical element buttons into a container.
func create_periodic_table():
	current_atom_no = 1
	#while current_atom_no < chemElementsData.size() # extended periodic table
	while current_atom_no <= 118:
		create_chem_element_button(current_atom_no)
		if current_atom_no == 1: create_button_gap(30) # 1+14+10+5
		elif current_atom_no == 4: create_button_gap(24) # 14+10
		elif current_atom_no == 12: create_button_gap(24) # 14+10
		elif current_atom_no == 20: create_button_gap(14)
		elif current_atom_no == 38: create_button_gap(14)
		current_atom_no += 1


func connect_button_signal(button : ChemElementButton):
	button.pressed.connect(
		_on_chemelementbutton_pressed.bind(button.chem_element))


func update_chem_element_button(button: ChemElementButton):
	button.text = chem_elements_data[button.chem_element][0]


func _on_chemelementbutton_pressed(atom_no : int):
	std_atom_wght_label.text = str(
			"Standard atomic weight: ",
			chem_elements_data[atom_no][std_atom_wght_id])
	name_label.text = str(
			"Element #", atom_no, ": ",
			chem_elements_data[atom_no][symbol_id], " ",
			chem_elements_data[atom_no][name_en_us_id])


func get_block(atom_no : int) -> int:
	if ( # s-block: 1s, 2s, 3s, 4s, 5s, 6s, 7s
		(atom_no >= 1 and atom_no <= 4) or atom_no == 11 or atom_no == 12 or
		atom_no == 19 or atom_no == 20 or atom_no == 37 or atom_no == 38 or
		atom_no == 55 or atom_no == 56 or atom_no == 87 or atom_no == 88):
		return 0
	elif ( # p-block: 2p, 3p, 4p, 5p, 6p, 7p
		atom_no >= 5 and atom_no <= 10) or (atom_no >= 13 and atom_no <= 18) or (
		atom_no >= 31 and atom_no <= 36) or (atom_no >= 49 and atom_no <= 54) or (
		atom_no >= 81 and atom_no <= 86) or (atom_no >= 113 and atom_no <= 118):
		return 1
	elif ( # d-block: 3d, 4d, 5d, 6d
		atom_no >= 21 and atom_no <= 30) or (atom_no >= 39 and atom_no <= 48) or (
		atom_no >= 71 and atom_no <= 80) or (atom_no >= 103 and atom_no <= 112):
		return 2
	elif ( # f-block: 4f, 5f
		atom_no >= 57 and atom_no <= 70) or (atom_no >= 89 and atom_no <= 102):
		return 3
	else: # unknown
		return -1


func get_block_color(block: int) -> Color:
	match block:
		0:
			return Color(0.847, 0.251, 0.251)
		1:
			return Color(0.847, 0.847, 0.188)
		2:
			return Color(0.408, 0.408, 0.878)
		3:
			return Color(0.157, 0.722, 0.157)
		_:
			return Color.WHITE


func scale_buttons(zoom : int):
	var min_size = Vector2(40, 40)
	if zoom < 0 and button_parent.get_child(0).custom_minimum_size <= min_size:
		zoom = 0
	for button in button_parent.get_children():
		button.custom_minimum_size += Vector2(zoom, zoom)
# TODO InputEventMagnifyGesture for mobile spread & pinch zooming


func _process(_delta): # _process(delta) gives warning
	if Input.is_action_just_pressed("ui_zoom_in"): scale_buttons(+1)
	if Input.is_action_just_pressed("ui_zoom_out"): scale_buttons(-1)


func _on_button_back_pressed():
	Global.change_scene_to_packed(Global.main_menu)
