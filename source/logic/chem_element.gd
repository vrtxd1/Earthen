## This file is a part of the Godot 4 source code of
## Earthen: Dirt Simulator https://vrtxd.wordpress.com/earthen
## Design: vrtxd (Juho Hartikainen) 2024-
## License: MIT https://opensource.org/license/MIT

class_name ChemElement
extends Node


enum Symbol {
	none, # Align values with atomic numbers
	H,  He, # 1s
	Li, Be, # 2s
	B,  C,  N,  O,  F,  Ne, # 2p
	Na, Mg, # 3s
	Al, Si, P,  S,  Cl, Ar, # 3p
	K,  Ca, # 4s
	Sc, Ti, V,  Cr, Mn, Fe, Co, Ni, Cu, Zn, # 3d
	Ga, Ge, As, Se, Br, Kr, # 4p
	Rb, Sr, # 5s
	Y,  Zr, Nb, Mo, Tc, Ru, Rh, Pd, Ag, Cd, # 4d
	In, Sn, Sb, Te, I,  Xe, # 5p
	Cs, Ba, # 6s
	La, Ce, Pr, Nd, Pm, Sm, Eu, Gd, Tb, Dy, Ho, Er, Tm, Yb, # 4f
	Lu, Hf, Ta, W,  Re, Os, Ir, Pt, Au, Hg, # 5d
	Tl, Pb, Bi, Po, At, Rn, # 6p
	Fr, Ra, # 7s
	Ac, Th, Pa, U,  Np, Pu, Am, Cm, Bk, Cf, Es, Fm, Md, No, # 5f
	Lr, Rf, Db, Sg, Bh, Hs, Mt, Ds, Rg, Cn, #6d
	Nh, Fl, Mc, Lv, Ts, Og, #7p
	Uue,Ubn, # 8s
	Ubu,Ubb,Ubt,Ubq,Ubp,Ubh,Ubs,Ubo,Ube,Utn,Utu, # 5g
	Utb,Utt,Utq,Utp,Uth,Uts,Uto,Ute,Uqn,Uqu,Uqb, # 5g
	Uqt,Uqq,Uqp,Uqh,Uqs,Uqo,Uqe,Upn,Upu,Upb,Upt,Upq,Upp,Uph, # 6f
	Ups,Upo,Upe,Uhn,Uhu,Uhb,Uht,Uhq,Uhp,Uhh, #7d
	Uhs,Uho,Uhe,Usn,Usu,Usb, #8p
}
