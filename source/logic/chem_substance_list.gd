## This file is a part of the Godot 4 source code of
## Earthen: Dirt Simulator https://vrtxd.wordpress.com/earthen
## Design: vrtxd (Juho Hartikainen) 2024-
## License: MIT https://opensource.org/license/MIT

class_name ChemSubstanceList
extends Node

## BBCode-enabled rich text label for listing chemical substances
@export var bbcode_table : RichTextLabel

var chem_substances_data: Array

var formulas_id : int
var names_en_us_id : int
var names_fi_fi_id : int


func _ready():
	chem_substances_data = DataFileAccess.new().read_csv_data(
			"res://data/chem_substances.csv")
	assign_data_ids(chem_substances_data)
	bbcode_table.text = create_bbcode_table()
	var new_atom := Atom.new()
	new_atom.read_formula(chem_substances_data[4][0][0])


func assign_data_ids (data : Array):
	var front : Array
	for col in data.front(): # unpack [["x"], ["y"]] to ["x", "y"]
		front.push_back(col[0])
	formulas_id = front.find("formulas")
	names_en_us_id = front.find("names_enUS")
	names_fi_fi_id = front.find("names_fiFI")


func create_bbcode_table () -> String:
	# TODO: heading translations
	var table_text: String = "[table=2 border]\n
[cell][b]formula[/b][/cell]\n[cell][b]substance[/b][/cell]\n"
	var front := true
	for chem_substance in chem_substances_data:
		if front: front = false # skip first row
		else:
			table_text = str(table_text,
"""
[cell]""", chem_substance[0][0], """[/cell]
[cell]""", chem_substance[1][0], """[/cell]
"""
			)
	table_text = str(table_text, "\n[/table]")
	return table_text


func _on_button_back_pressed():
	Global.change_scene_to_packed(Global.main_menu)


## NOTE: Molecular structures would require e.g. implementing an interpreter
## for simplified molecular-input line-entry system (SMILES) notation and
## procedurally generating 3D models based on that data. Meanwhile, there are
## no plans to design game mechanics for playing molecular structures, so this
## would only be done for visualization purposes. No need for now.
