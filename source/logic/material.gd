extends Node


var label
var volume
var mass
var temperature
var pressure
var dispersion_materials : Array


func new(label, volume, mass, temperature, pressure, dispersion_materials):
	label = self.label
	volume = self.volume
	mass = self.mass
	temperature = self.temperature
	pressure = self.pressure
	dispersion_materials = self.dispersion_materials
