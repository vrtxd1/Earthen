## This file is a part of the Godot 4 source code of
## Earthen: Dirt Simulator https://vrtxd.wordpress.com/earthen
## Design: vrtxd (Juho Hartikainen) 2024-
## License: MIT https://opensource.org/license/MIT

class_name SubAtom
extends Node


enum Particle { proton, neutron, electron, photon }

@export var current_particle := SubAtom.Particle.electron

var subatom_particles_data : Array
var symbol_id : int
var mass_id : int
var electric_charge_id : int
var name_en_id : int
var name_fi_id : int

var mass: float
var electric_charge: int
var symbol: String
var label: String


func _ready():
	subatom_particles_data = DataFileAccess.new().read_csv_data(
		"res://data/subatom_particles.csv")
	assign_data_ids(subatom_particles_data.front())
	assign_data(current_particle)
	create_button(current_particle)
	print(str(electric_charge, ", ", mass, ", ", symbol, ", ", label))


func assign_data_ids (front : Array):
	symbol_id = front.find("symbol")
	mass_id = front.find("mass")
	name_en_id = front.find("name_enUS")
	name_fi_id = front.find("name_fiFI")
	electric_charge_id = front.find("electricCharge")


func assign_data (particle : SubAtom.Particle):
	var dataIndex = Particle.keys().find(Particle.keys()[particle])
	var data = subatom_particles_data[dataIndex+1]
	mass = data[mass_id] as float
	electric_charge = data[electric_charge_id] as int
	symbol = data[symbol_id]
	label = data[name_en_id]


func create_button(particle: Particle) -> Button:
	var button : Button
	current_particle = particle
	var data_id = Particle.keys().find(Particle.keys()[particle])
	var data = subatom_particles_data[data_id+1]
	label = Particle.keys()[particle]
	self.text = str(data[symbol_id])
	return button
