class_name DataFileAccess
extends Node


func _ready():
	pass


# Reads data from a .csv file path and returns the data as an array.
func read_csv_data(path : String):
	var data : Array = []
	var file = FileAccess.open(path, FileAccess.READ)
	while not file.eof_reached():
		var line: PackedStringArray = []
		var row : Array = []
		line = file.get_csv_line(";")
		for cell: String in line:
			var variants : PackedStringArray = []
			variants = cell.split(",", true, 0)
			row.append(variants)
		if line.size() > 1:
			data.append(row)
	file.close()
	return data


func split_csv_cell(line : String) -> PackedStringArray:
	var data : PackedStringArray
	data = line.split(",", true, 0)
	return data
