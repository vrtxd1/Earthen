## This file is a part of the Godot 4 source code of
## Earthen: Dirt Simulator https://vrtxd.wordpress.com/earthen
## Design: vrtxd (Juho Hartikainen) 2024-
## License: MIT https://opensource.org/license/MIT

class_name MainMenu
extends Control


@export var elements_scene: PackedScene
@export var substances_scene: PackedScene
@export var version: Label


func _ready():
	version.text = ProjectSettings.get_setting("application/config/version")


func _on_button_elements_pressed():
	Global.change_scene_to_packed(elements_scene)


func _on_button_substances_pressed():
	Global.change_scene_to_packed(substances_scene)


func _on_button_theme_pressed():
	Global.toggle_gui_theme()


func _on_button_quit_pressed():
	get_tree().quit()
